package org.xelevra.prefdata.annotations;

public @interface Keyword {
    String value();
}
